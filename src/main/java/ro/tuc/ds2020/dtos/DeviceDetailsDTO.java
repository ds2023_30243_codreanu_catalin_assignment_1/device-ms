package ro.tuc.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class DeviceDetailsDTO {

    private UUID id;
    @NotNull
    private UUID userId;
    @NotNull
    private String description;
    @NotNull
    private String address;
    @NotNull
    private int maximumHourlyEnergyConsumption;

    public DeviceDetailsDTO() {
    }

    public DeviceDetailsDTO(UUID userId, String description, String address, int maximumHourlyEnergyConsumption) {
        this.id = UUID.randomUUID();
        this.userId = userId;
        this.description = description;
        this.address = address;
        this.maximumHourlyEnergyConsumption = maximumHourlyEnergyConsumption;
    }

    public DeviceDetailsDTO(UUID id, UUID userId, String description, String address, int maximumHourlyEnergyConsumption) {
        this.id = id;
        this.userId = userId;
        this.description = description;
        this.address = address;
        this.maximumHourlyEnergyConsumption = maximumHourlyEnergyConsumption;
    }

    public UUID getId() {
        return id;
    }

    public UUID getUserId() {
        return userId;
    }

    public String getDescription() {
        return description;
    }

    public String getAddress() {
        return address;
    }

    public int getMaximumHourlyEnergyConsumption() {
        return maximumHourlyEnergyConsumption;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setMaximumHourlyEnergyConsumption(int maximumHourlyEnergyConsumption) {
        this.maximumHourlyEnergyConsumption = maximumHourlyEnergyConsumption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceDetailsDTO that = (DeviceDetailsDTO) o;
        return Objects.equals(description, that.description) &&
                Objects.equals(address, that.address) &&
                Objects.equals(maximumHourlyEnergyConsumption, that.maximumHourlyEnergyConsumption);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, address, maximumHourlyEnergyConsumption);
    }
}
