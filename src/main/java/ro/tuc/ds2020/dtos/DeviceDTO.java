package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;
import java.util.UUID;

public class DeviceDTO extends RepresentationModel<DeviceDTO> {
    private UUID id;
    private UUID userId;
    private String description;
    private String address;
    private int getMaximumHourlyEnergyConsumption;

    public DeviceDTO(UUID id, UUID userId, String description, String address, int getMaximumHourlyEnergyConsumption) {
        this.id = id;
        this.userId = userId;
        this.description = description;
        this.address = address;
        this.getMaximumHourlyEnergyConsumption = getMaximumHourlyEnergyConsumption;
    }

    public UUID getId() {
        return id;
    }

    public UUID getUserId() {
        return userId;
    }

    public String getDescription() {
        return description;
    }

    public String getAddress() {
        return address;
    }

    public int getGetMaximumHourlyEnergyConsumption() {
        return getMaximumHourlyEnergyConsumption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceDTO deviceDTO = (DeviceDTO) o;
        return
                Objects.equals(description, deviceDTO.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, getMaximumHourlyEnergyConsumption);
    }
}
