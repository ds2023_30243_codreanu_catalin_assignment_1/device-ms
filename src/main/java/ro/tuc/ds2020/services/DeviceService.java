package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.DeviceDetailsDTO;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.repositories.DeviceRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DeviceService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceService.class);
    private final DeviceRepository deviceRepository;

    @Autowired
    public DeviceService(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    public List<DeviceDTO> findDevices() {
        List<Device> deviceList = deviceRepository.findAll();
        return deviceList.stream()
                .map(DeviceBuilder::toDeviceDTO)
                .collect(Collectors.toList());
    }

    public List<DeviceDTO> findDevicesByUserId(UUID userId) {
        List<Device> deviceList = deviceRepository.findAll();

        return deviceList.stream()
                .filter(d -> d.getUserId().equals(userId))
                .map(DeviceBuilder::toDeviceDTO)
                .collect(Collectors.toList());
    }

    public DeviceDetailsDTO findDeviceById(UUID id) {
        Optional<Device> prosumerOptional = deviceRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Device with id {} was not found in db", id);
            throw new ResourceNotFoundException(Device.class.getSimpleName() + " with id: " + id);
        }
        return DeviceBuilder.toDeviceDetailsDTO(prosumerOptional.get());
    }

    public UUID insert(DeviceDetailsDTO deviceDTO) {
        Device device = DeviceBuilder.toEntity(deviceDTO);

        device.checkId();
        device = deviceRepository.save(device);

        LOGGER.debug("Device with id {} was inserted in db", device.getId());

        return device.getId();
    }

    public void delete(DeviceDetailsDTO deviceDTO) {
        Device device = DeviceBuilder.toEntity(deviceDTO);
        deviceRepository.delete(device);

        LOGGER.debug("Device with id {} was deleted from db", device.getId());

        return;
    }

    public void updateDevice(DeviceDetailsDTO deviceDTO) {
        DeviceDetailsDTO currentDevice = findDeviceById(deviceDTO.getId());
        String newDescription = deviceDTO.getDescription();

        if(!deviceDTO.getAddress().equals("-1")){
            currentDevice.setAddress(deviceDTO.getAddress());
        }

        if(deviceDTO.getMaximumHourlyEnergyConsumption() != -1){
            currentDevice.setMaximumHourlyEnergyConsumption(deviceDTO.getMaximumHourlyEnergyConsumption());
        }

        if(!newDescription.equals("-1")){
            currentDevice.setDescription(newDescription);
        }

        Device updatedDevice = DeviceBuilder.toEntity(currentDevice);

        deviceRepository.save(updatedDevice);
    }
}
