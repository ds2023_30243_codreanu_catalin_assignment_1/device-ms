# Use an official OpenJDK runtime as a parent image
FROM openjdk:17

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy the application JAR file into the container
COPY target/ds-2020-0.0.1-SNAPSHOT.jar .

# Expose the port the app runs on
EXPOSE 8081

# Run the application
CMD ["java", "-jar", "ds-2020-0.0.1-SNAPSHOT.jar"]
